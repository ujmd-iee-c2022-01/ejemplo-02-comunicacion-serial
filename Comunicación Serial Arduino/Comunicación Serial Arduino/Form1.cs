﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;              // NameSpace con puertos de Entrada y Salida de la PC

namespace Comunicación_Serial_Arduino
{
    public partial class Form1 : Form
    {
        //Espacio para declaración de variables y constantes globales para las funciones del formulario
        string DistanciaCM = "", DistanciaIN = "";
        public Form1()
        {
            InitializeComponent();
        }

        private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            // Al recibir información, si hay una línea disponible, se lee con el
            // método ReadLine()
            string Lectura = serialPort1.ReadLine();

            if (Lectura.Length > 0)
            {
                string[] partes = Lectura.Split(':','\r');
                if (partes[0] == "distance_cm")
                {
                    DistanciaCM = partes[1];
                }
                else if (partes[0] == "distance_in")
                {
                    DistanciaIN = partes[1];
                }
            }
        }


        private void btnAbrir_Click(object sender, EventArgs e)
        {
            try
            {
                serialPort1.PortName = cmbPuertos.Text;
                serialPort1.Open();
                label1.Text = "Puerto " + serialPort1.PortName + " abierto";
            }
            catch (Exception ex)
            {
                label1.Text = "No se pudo abrir el puerto. Detalles:\n" + ex.Message;
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                serialPort1.Close();
                label1.Text = "Puerto " + serialPort1.PortName + " cerrado";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();

            foreach (var item in ports)
            {
                cmbPuertos.Items.Add(item);
            }

            if (cmbPuertos.Items.Count > 0)
            {
                cmbPuertos.SelectedIndex = 0;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            txtDistanciaCM.Text = DistanciaCM;
            txtDistanciaIN.Text = DistanciaIN;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {

            if (serialPort1.IsOpen)
            {
                serialPort1.Close();
            }
        }
    }
}
