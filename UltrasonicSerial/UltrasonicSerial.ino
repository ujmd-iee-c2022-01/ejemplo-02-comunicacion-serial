#include <Ultrasonic.h>

Ultrasonic ultrasonic(7, 8);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  int distance_cm = ultrasonic.read(CM);
  int distance_in = ultrasonic.read(INC);

  Serial.print("distance_cm:");
  Serial.println(distance_cm);

  Serial.print("distance_in:");
  Serial.println(distance_in);
  delay(2000);
}
